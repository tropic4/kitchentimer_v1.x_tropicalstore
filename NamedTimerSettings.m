//
//  NamedTimerSettings.m
//  KitchenTimer
//
//  Created by Lewis Garrett on 2/22/11.
//  Copyright 2011 Iota. All rights reserved.
//

#import "NamedTimerSettings.h"
#import "KitchenTimerDocumentController.h"				//leg20110309 - 1.0.1

@implementation NamedTimerSettings

@synthesize timerNameTextField, timerSoundPopupButton;

/*
-(id) init {
	self = [super initWithWindowNibName:@"NamedTimerSettings"];
	return self;
}
*/

-(void)awakeFromNib {
	// Establish new named timer defaults
//	NSString *timerNameText = [[NSUserDefaults standardUserDefaults] stringForKey:@"TimerName"];
	
	if ([[NSUserDefaults standardUserDefaults] stringForKey:@"TimerName"] != nil) {
		[[self timerNameTextField] setStringValue: [[NSUserDefaults standardUserDefaults]
			stringForKey:@"TimerName"]];
	}
	
	if ([[NSUserDefaults standardUserDefaults] stringForKey:@"SoundName"] != nil) {
		[[self timerSoundPopupButton] selectItemWithTitle: [[NSUserDefaults standardUserDefaults]
			stringForKey:@"SoundName"]];
	}
	
	[[self window] center];
}

-(IBAction)createTimerButtonAction:(id)sender {
	// Retreive new named timer options
	NSMutableString *timerName = (NSMutableString *)[timerNameTextField stringValue];
	
	// Guard against no name entered.
	if (timerName != nil && ![@"" isEqual: timerName]) {
		[[NSUserDefaults standardUserDefaults]
			setObject:timerName forKey:@"TimerName"];
	}

	NSString *timerSound = [timerSoundPopupButton titleOfSelectedItem];
	[[NSUserDefaults standardUserDefaults]
		setObject:timerSound forKey:@"SoundName"];

	BOOL isNamedTimer = YES;
	[[NSUserDefaults standardUserDefaults]
		setBool:isNamedTimer forKey:@"IsNamedTimer"];

	[[NSUserDefaults standardUserDefaults] synchronize];
		
	[[self window] close];
	
	// Create a new timer document.  Timer document will use options stored in standardUserDefaults.
	//[[NSDocumentController sharedDocumentController] newDocument:self];
	[[KitchenTimerDocumentController sharedDocumentController] newDocument:sender];			//leg20110309 - 1.0.1
}

-(IBAction)cancelButtonAction:(id)sender {
	// New named timer cancelled, restore defaults
	if ([[NSUserDefaults standardUserDefaults] stringForKey:@"TimerName"] != nil) {
		[[self timerNameTextField] setStringValue: [[NSUserDefaults standardUserDefaults]
			stringForKey:@"TimerName"]];
	}
	
	if ([[NSUserDefaults standardUserDefaults] stringForKey:@"SoundName"] != nil) {
		[[self timerSoundPopupButton] selectItemWithTitle: [[NSUserDefaults standardUserDefaults]
			stringForKey:@"SoundName"]];
	}
	
	[self close];
}

-(IBAction)timerNameTextFieldAction:(id)sender {
	//NSString *timerName = [timerNameTextField stringValue];
	//[self close];
}


@end
