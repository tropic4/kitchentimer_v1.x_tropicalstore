//
//  ActivateProductWindowController.h
//  KitchenTimer
//
//  Created by Lewis Garrett on 5/22/14.
//
//

#import <Cocoa/Cocoa.h>

@interface ActivateProductWindowController : NSWindowController {
    NSTextField *nameTextField;
    NSTextField *companyTextField;
    NSTextField *productKeyTextField;
    IBOutlet NSButtonCell *cancelButtonCell;
    IBOutlet NSButtonCell *activateButtonCell;

	NSMutableDictionary *savedSettingsDictionary;
    
	NSString* productCodeOwnerNameText;
	NSString* completeProductCodeText;
}

// Product Registration properties.                                             //leg20240411 - TS-1.3.0
@property (nonatomic, strong) NSMutableString *registeredProductCode;
@property (nonatomic, strong) IBOutlet NSMutableString *registeredName;
@property (nonatomic, strong) IBOutlet NSMutableString *registeredCompany;

@property (assign) IBOutlet NSTextField *nameTextField;
@property (assign) IBOutlet NSTextField *companyTextField;
@property (assign) IBOutlet NSTextField *productKeyTextField;

-(IBAction)activateButtonAction:(id)sender;
-(IBAction)cancelButtonAction:(id)sender;

@end
