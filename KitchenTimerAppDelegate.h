//
//  KitchenTimerAppDelegate.h
//  KitchenTimer
//
//  Created by Lewis Garrett on 3/7/11.
//  Copyright 2011 Iota. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "KitchenTimerDocumentController.h"

@interface KitchenTimerAppDelegate : NSObject <NSApplicationDelegate> {
    NSWindow *window;
    KitchenTimerDocumentController *kitchenTimerDocumentController;
}

@property (assign) IBOutlet NSWindow *window;
@property (nonatomic, strong) NSMutableString *registeredProductCode;
@property (nonatomic, strong) IBOutlet NSMutableString *registeredName;
@property (nonatomic, strong) IBOutlet NSMutableString *registeredCompany;

@end
