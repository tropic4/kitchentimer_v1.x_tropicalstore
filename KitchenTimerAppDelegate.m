//
//  KitchenTimerAppDelegate.m
//  KitchenTimer
//
//  Created by Lewis Garrett on 3/7/11.
//  Copyright 2011 Iota. All rights reserved.
//

#import <Sparkle/Sparkle.h>
#import "KitchenTimerAppDelegate.h"
#import "KitchenTimerDocumentController.h"

@implementation KitchenTimerAppDelegate

@synthesize window;

- (void)applicationWillFinishLaunching:(NSNotification *)aNotification {
	// Insert code here to initialize your application 
	kitchenTimerDocumentController = [[KitchenTimerDocumentController alloc] init];
	[kitchenTimerDocumentController release];
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
	// Insert code here to initialize your application 

        
    // Prevent KitchenTimer multiple launches.                                  //leg20201229 - 1.2.0
    //
    // Hsoi 2014-10-19 - Determine if app is running (again) and terminate this instance if so.
    // This code is taken directly from DTS Follow-up: 612125399. See https://bitbucket.org/iaddressx/iaddressx/issue/27/iaddressx-is-running-twice
    // for discussion of greater issues that still need addressing.
    //
    NSArray*    apps = [NSRunningApplication runningApplicationsWithBundleIdentifier:[[NSBundle mainBundle] bundleIdentifier]];
    if ([apps count] > 1) {
        NSRunningApplication* curApp = [NSRunningApplication currentApplication];
        for (NSRunningApplication* app in apps) {
            if ([[app bundleIdentifier] isEqualToString:[curApp bundleIdentifier]]) {
                NSAlert *testAlert = [NSAlert alertWithMessageText:NSLocalizedString(@"KitchenTimer is Already Running.", nil)
                                                     defaultButton:NSLocalizedString(@"Quit", nil)
                                                   alternateButton:nil
                                                       otherButton:nil
                                         informativeTextWithFormat:NSLocalizedString(@"Only one version of KitchenTimer can be run by a user. This instance must quit.", nil)];
                [testAlert runModal];
                
                [app activateWithOptions:NSApplicationActivateAllWindows | NSApplicationActivateIgnoringOtherApps];
                
                [NSApp terminate:nil];
                return;
            }
        }
    }
    
// Experimental customization of Sparkle Updating
//    NSTimeInterval ck4UpdateTimeInterval = [[SUUpdater sharedUpdater] updateCheckInterval];
//    [[SUUpdater sharedUpdater] setAutomaticallyChecksForUpdates:YES];
//    [SUUpdater sharedUpdater].delegate = self;
//
//    [[SUUpdater sharedUpdater] checkForUpdates:nil];

    // *** DEBUGGING CODE ***
    if (NO) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"RegisteredProductCode"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"RegisteredName"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"RegisteredCompany"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"IsDisabled"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"FirstRunDate"];
        [NSUserDefaults resetStandardUserDefaults];
    }
    
    _registeredProductCode = (NSMutableString *)[[NSUserDefaults standardUserDefaults] stringForKey:@"RegisteredProductCode"];    //leg20140521 - 1.1.0
    _registeredName = (NSMutableString *)[[NSUserDefaults standardUserDefaults] stringForKey:@"RegisteredName"];                  //leg20140521 - 1.1.0
    _registeredCompany = (NSMutableString *)[[NSUserDefaults standardUserDefaults] stringForKey:@"RegisteredCompany"];            //leg20140521 - 1.1.0

    NSDate *firstRunDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"FirstRunDate"];
    NSTimeInterval timeSinceFirstRun = 0.0;
    timeSinceFirstRun = [[NSDate date] timeIntervalSinceDate:firstRunDate];
    if (!_registeredProductCode) {
        if (![[NSUserDefaults standardUserDefaults] boolForKey:@"IsDisabled"]) {
            if (!firstRunDate) {
                [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"FirstRunDate"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                NSAlert *alert = [NSAlert alertWithMessageText:@"KitchenTimer not registered!" defaultButton:nil alternateButton:nil otherButton:nil informativeTextWithFormat:@"KitchenTimer must be registered within 30 days of first use!"];
                [alert runModal];
            } else {
                
                // Disable "Check For Updates" menu.
                NSMenu *menu = [[NSApplication sharedApplication] mainMenu];
                NSMenuItem *supportMenuItem = [menu itemWithTitle:@"Support"];
                NSMenu *supportSubMenu = [supportMenuItem submenu];
                [supportSubMenu setAutoenablesItems:NO];
                NSMenuItem *ck4UpdatesMenuItem = [supportSubMenu itemWithTitle:@"Check For Updates"];
                [ck4UpdatesMenuItem setEnabled:NO];

                //        if (timeSinceFirstRun > 60*60*24*30 && !_registeredProductCode) {
                if (timeSinceFirstRun > 60*5 && !_registeredProductCode) {
                    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:YES] forKey:@"IsDisabled"];
                    [[NSUserDefaults standardUserDefaults] synchronize];

                    NSAlert *alert = [NSAlert alertWithMessageText:@"30 days of free use has expired." defaultButton:nil alternateButton:@"Activate Kitchentimer" otherButton:@"Buy Kitchentimer" informativeTextWithFormat:@"KitchenTimer must be registered for continued use!"];
                    NSInteger buttonIndex = [alert runModal];
                    
                    if (buttonIndex == -1)
                        [kitchenTimerDocumentController jumpToTropic4BuyPage:nil];
                    else if (buttonIndex == 0)
                        [kitchenTimerDocumentController showActivateProductKeyWindow:nil];
                } else {
                    //            NSInteger daysSinceFirstRun = timeSinceFirstRun/86400.0;
                    //            NSInteger daysLeftB4Expire = 30 - daysSinceFirstRun;
                    NSInteger daysSinceFirstRun = timeSinceFirstRun/60;
                    NSInteger daysLeftB4Expire = 5 - daysSinceFirstRun;
                    NSAlert *alert = [NSAlert alertWithMessageText:@"KitchenTimer not registered!" defaultButton:nil alternateButton:nil otherButton:nil informativeTextWithFormat:@"KitchenTimer must be registered within %d days!", daysLeftB4Expire];
                    [alert runModal];
                }
            }
        } else {
            // 30 days of free use has expired, Disable "Check For Updates" menu.
            NSMenu *menu = [[NSApplication sharedApplication] mainMenu];
            NSMenuItem *supportMenuItem = [menu itemWithTitle:@"Support"];
            NSMenu *supportSubMenu = [supportMenuItem submenu];
            [supportSubMenu setAutoenablesItems:NO];
            NSMenuItem *ck4UpdatesMenuItem = [supportSubMenu itemWithTitle:@"Check For Updates"];
            [ck4UpdatesMenuItem setEnabled:NO];
            
            NSAlert *alert = [NSAlert alertWithMessageText:@"30 days of free use has expired." defaultButton:nil alternateButton:@"Activate Kitchentimer" otherButton:@"Buy Kitchentimer" informativeTextWithFormat:@"Go to Activate menu to register KitchenTimer!"];
            NSInteger buttonIndex = [alert runModal];

            if (buttonIndex == -1)
                [kitchenTimerDocumentController jumpToTropic4BuyPage:nil];
            else if (buttonIndex == 0)
                [kitchenTimerDocumentController showActivateProductKeyWindow:nil];
        }
    }
}

// Experimental customization of Sparkle Updating
//- (void)updater:(SUUpdater *)updater didFinishLoadingAppcast:(SUAppcast *)appcast {
//    int x = 47;
//}
//
//- (void)updater:(SUUpdater *)updater didFindValidUpdate:(SUAppcastItem *)update {
//    int x = 47;
//}
//
//- (void)updaterDidNotFindUpdate:(SUUpdater *)update {
//    int x = 47;
//}
//
//- (id <SUVersionComparison>)versionComparatorForUpdater:(SUUpdater *)updater {
//    int x = 47;
//    return NSOrderedDescending;
//}

@end
