//
//  TimerDocument.m
//  KitchenTimer
//
//  Created by Lewis Garrett on Fri May 30 2003.
//  Copyright (c) 2003 Iota. All rights reserved.
//

#import "TimerDocument.h"
#import "NamedTimerSettings.h"

// Global variables
extern int gTimerNumber;

@implementation TimerDocument

- (id)init
{
    self = [super init];
    if (self) {
    
        // Add your subclass-specific initialization here.
        // If an error occurs here, send a [self release] message and return nil.
        
        // initialize timer values
        secondsTime = 0;
        minutesTime = 0;
        hoursTime = 0;
        lastCountDownTime = 0;
        
        // Establish defaults for named timers        //leg20110222 - 1.0.1
        timerName = (NSMutableString*)[[NSUserDefaults standardUserDefaults] stringForKey:@"TimerName"];
        timerSound = (NSMutableString*)[[NSUserDefaults standardUserDefaults] stringForKey:@"SoundName"];
        isNamedTimer = [[NSUserDefaults standardUserDefaults] boolForKey:@"IsNamedTimer"];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil timerName:(NSString*)name timerSound:(NSString*)sound {    //leg20110222 - 1.0.1
    
    //if (self = [super initWithNibName:nibNameOrNil bundle:nil]) {
    if (self = [super init]) {
    }
    [self windowControllerDidLoadNib: nil];
    return self;
}

- (NSString *)windowNibName
{
    // Override returning the nib file name of the document
    // If you need to use a subclass of NSWindowController or if your document supports multiple NSWindowControllers, you should remove this method and override -makeWindowControllers instead.
    return @"TimerWindow";
}

- (void)windowControllerDidLoadNib:(NSWindowController *) aController
{
    [super windowControllerDidLoadNib:aController];
    // Add any code here that needs to be executed once the windowController has loaded the document's window.

    // Make the window controller object handy for later.
    myWindowController = aController;

    // Disable timer if product not registeredl.                                //leg20140521 - 1.1.0
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"IsDisabled"]) {
        [startStopButton setEnabled:NO];
        [resetButton setEnabled:NO];
    }

    // See if this timer document will have a name and sound provided by the user
    if (!isNamedTimer) {
        // This is not a named timer

        // Set the name of the sound to use on timer expiration
        timerSound = (NSMutableString*)@"Glass";

        //NSString *timerName = [NSString stringWithFormat:@"Timer %d", gTimerNumber++];
        NSString *timerNameString = [NSString stringWithFormat:@"Timer %d", gTimerNumber++];
        NSString *token;
        NSString *documentName;

        // Get the name of the file ("Untitled").  Could not use "- (NSString *)fileName;"
        //  because that method returns nil if file is untitled.
        documentName = [self displayName];

        // Break displayName into tokens in case this is not the first instance of TimerDocument.
        NSArray *listItems = [documentName componentsSeparatedByString:@" "];
        
        // Isolate "Untitled" from displayName
        token = [listItems objectAtIndex:0];

        // Put displayName in an NSMutableString so that we can alter it.
        windowTitle = [NSMutableString stringWithString:documentName];

        // Change "Untitled" to "Timer" leaving the number suffix (if any).
        [windowTitle replaceOccurrencesOfString:token
                                                withString:timerNameString
                                                options:NSLiteralSearch
                                                range:NSMakeRange(0, [windowTitle length])];
    } else {
        // This is a named timer                                //leg20110222 - 1.0.1

        // Get the name and sound for the timer
        timerName = (NSMutableString*)[[NSUserDefaults standardUserDefaults] stringForKey:@"TimerName"];
        if (timerName == nil || [@"" isEqual: timerName])
            timerName = (NSMutableString*)@" ";

        // Name the timer window.
        windowTitle = timerName;
            
        timerSound = (NSMutableString*)[[NSUserDefaults standardUserDefaults] stringForKey:@"SoundName"];
        if (timerSound == nil || [@"" isEqual: timerSound])
            timerSound = (NSMutableString*)@"Glass";

        // Make sure named timer flag is set off.
        [[NSUserDefaults standardUserDefaults]
            setBool:NO forKey:@"IsNamedTimer"];
        [[NSUserDefaults standardUserDefaults] synchronize];    //leg20110309 - 1.0.1

    }
    
    // Put the first timer at the upper left of the screen        //leg20110309 - 1.0.1
    if (gTimerNumber == 2) {
        NSPoint point;
        point.x = 30.0;
        point.y = 30.0;
        point = [[myWindowController window] cascadeTopLeftFromPoint: point];
    }

    // Set fileName to the "changed" name.  This will cause the document
    //  name (and the window title) to be synchronized with the fileName.
    NSString *urlString = [NSString stringWithFormat:@"file:///%@", windowTitle];
    NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    [self setFileURL:url];

    // Slow down the typomatic (continuous) effect of the hours,
    //  minutes, and seconds buttons
    float delay = 0.4;
    float interval = 0.150;
    [hoursButton setPeriodicDelay:delay interval:interval];
    [minutesButton setPeriodicDelay:delay interval:interval];
    [secondsButton setPeriodicDelay:delay interval:interval];
    
    // After the window appears, update the display
    [self updateTimerDisplay];
}

- (void)windowWillClose:(NSNotification *)notification
{
    if (timerIsRunning) {
        // Invalidate and release the timer
        [timer invalidate];
        [timer release];
    }
}

- (void)dealloc
{
    [namedTimerSettings release];                //leg20110222 - 1.0.1
    
    [super dealloc];
}

//
//� Action methods
//
- (IBAction)incrSeconds:(id)sender
{
    secondsTime++;
    if (secondsTime > 59)
        secondsTime = 0;
        
    [self updateTimerDisplay];
}

- (IBAction)incrMinutes:(id)sender
{
    minutesTime++;
    if (minutesTime > 59)
        minutesTime = 0;
        
    [self updateTimerDisplay];
}

- (IBAction)incrHours:(id)sender
{
    hoursTime++;
    if (hoursTime > 23)
        hoursTime = 0;
        
    [self updateTimerDisplay];
}

- (IBAction)startStopTimer:(id)sender
{
    IONotificationPortRef    notify;
    io_object_t         anIterator;

    // First press of start/stop button:  start timer.
    if ([sender state] == 1) {
        timerJustCountedDown = NO;
        timerIsRunning = YES;
        lastCountDownTime = (hoursTime*3600)+(minutesTime*60)+secondsTime;
        
        // Clear status display.
        [statusDisplay setObjectValue:@" "];

        // If timer setting is zero then we are counting up instead of down.
        if (lastCountDownTime == 0)
            timerIsCountingUp = YES;
        
        // Create a timer.
        //  Increase accuracy of timer by updating about 3 times a second       //leg20240403 - AS-1.3.0
        //  instead of once a second.
        timer = [[NSTimer scheduledTimerWithTimeInterval:0.3333
                                        target:self
                                        selector:@selector(seeIfTimeIsUp:)
                                        userInfo:nil
                                        repeats:YES] retain];

        // Set duration of count-down timer, or 0 for count-up timer.           //leg20240403 - AS-1.3.0
        timerStartDate = [NSDate date];
        timerDateInterval = [[NSDateInterval alloc]
                                         initWithStartDate:timerStartDate
                                                  duration:lastCountDownTime];

        // Disable all buttons except start/stop while timer is running.
        [resetButton setEnabled:NO];
        [hoursButton setEnabled:NO];
        [minutesButton setEnabled:NO];
        [secondsButton setEnabled:NO];

    // Second press of start/stop button:  stop timer.
    } else {
        timerJustCountedDown = YES;
        timerIsRunning = NO;
        timerHasExpired = NO;
        timerIsCountingUp = NO;
        
        // Re-enable all buttons except start/stop when timer has been stoped.
        [resetButton setEnabled:YES];
        [hoursButton setEnabled:YES];
        [minutesButton setEnabled:YES];
        [secondsButton setEnabled:YES];

        // Invalidate and release the timer
        [timer invalidate];
        [timer release];
    }
    
    [self updateTimerDisplay];
}

-(IBAction)resetTimer:(id)sender
{
    long temporary;                                                             //leg20240403 - AS-1.3.0
    
    // If the timer just counted down then set the timer display to the
    //  values used for the last count down, else zero the timer values.
    if (timerJustCountedDown) {
        // Turn count down flag off
        timerJustCountedDown = NO;

        // Break lastCountDownTime into hours, minutes, and seconds.
        hoursTime = lastCountDownTime/3600;
        temporary = (lastCountDownTime - (hoursTime*3600));
        minutesTime = temporary/60;
        secondsTime = (temporary - (minutesTime*60));
    } else {
        secondsTime = 0;
        minutesTime = 0;
        hoursTime = 0;
    }
    
    [self updateTimerDisplay];
}

// Go to Tropical Software's web site.
- (IBAction)openWebSiteInDefaultBrowser:(id)sender
{
  [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"http://www.tropic4.com"]];
}


// Throw up window to set the name of the Timer                //leg20110222 - 1.0.1
-(IBAction)showNamedTimerSettingsWindow:(id)sender
{
    // Lazy instantiation of controller
    if (!namedTimerSettings) {
        namedTimerSettings = [[NamedTimerSettings alloc] initWithWindowNibName:@"NamedTimerSettings"];
    }
    [namedTimerSettings showWindow:sender];
    [[namedTimerSettings window] center];
}

// Set the timer display.
- (void)updateTimerDisplay
{
    NSCalendarDate *yearZero;

    // Create a date representing the year 0 Common Era.
    yearZero = [NSCalendarDate calendarDate];
    [yearZero initWithYear:0 month:0 day:0 hour:0 minute:0 second:0 timeZone:0];
    
    // Add our timer value to the zero year so that date will represent only
    //     the hours, minutes and seconds of our timer.
    NSCalendarDate *timerValue = [yearZero dateByAddingYears:0
                                    months:0
                                    days:0
                                    hours:hoursTime
                                    minutes:minutesTime
                                    seconds:secondsTime];

    // NSDateFormatter will display timer value in the form:  HH:MM:SS.
    [timerDisplay setObjectValue:timerValue];
    
    // Update status display.
    if (timerHasExpired)
        [statusDisplay setObjectValue:@"** TIME IS UP **"];
    else if (timerIsRunning)
        if (timerIsCountingUp)
            [statusDisplay setObjectValue:@"** COUNT-UP **"];
        else
            [statusDisplay setObjectValue:@"** COUNT-DOWN **"];
    else
        [statusDisplay setObjectValue:@" "];
}

//
//� Private methods
//
// Modify so that timer ticks are no longer counted every time the              //leg20240403 - AS-1.3.0
//  Timer fires, but rather the elapsed time is now calculated whenever
//  the Timer fires (3 times per second,) and the display is updated. This
//  makes the timer more accurate, and it no longer loses time if the Mac
//  is put to sleep.
- (void)seeIfTimeIsUp:(NSTimer *)aTimer
{
    long secondsRemaining, temporary, displaySeconds = 0;                       //leg20240403 - AS-1.3.0
    NSDateInterval *elapsedDateInterval = nil;                                  //leg20240403 - AS-1.3.0
    
    // Calculate the number of seconds left before timer expires, or how
    //  many seconds since starting to count-up (or timer elapsed.)
    elapsedDateInterval = [[NSDateInterval alloc]
                           initWithStartDate:timerStartDate
                           endDate:[NSDate date]];

    secondsRemaining = timerDateInterval.duration - elapsedDateInterval.duration;

    // Reverse sign of timer display count if counting up.                      //leg20240403 - AS-1.3.0
    displaySeconds = (secondsRemaining < 0) ? -secondsRemaining : secondsRemaining;

    // Break secondsRemaining into hours, minutes, and seconds.                 //leg20240403 - AS-1.3.0
    hoursTime = displaySeconds/3600;
    temporary = (displaySeconds - (hoursTime*3600));
    minutesTime = temporary/60;
    secondsTime = (temporary - (minutesTime*60));
        
    // If seconds <= zero, flag the timer as having expired and load up the alarm sound.
    if ((secondsRemaining <= 0) && !timerIsCountingUp) {
        timerHasExpired = YES;
        alarmSound = [NSSound soundNamed:timerSound];    //leg20110222 - 1.0.1

        // De-miniaturize window (if it is miniaturized).
        [[myWindowController window] deminiaturize: nil];
    }

    // If timer has expired, signal to user by beeping every second.
    if (timerHasExpired) {
        // Fix TS Issue #005 - Crash after timer elapsed and sound played once  //leg20201229 - 1.2.0
        //  Load sound for each play instead of re-playing alarmSound.
        [[NSSound soundNamed:timerSound] play];
     }

    [self updateTimerDisplay];
}

#pragma mark Menu Management
//leg20110307 - 1.0.
- (BOOL)validateMenuItem:(NSMenuItem *)item {
    if ([item action] == @selector(showNamedTimerSettingsWindow:)) {
        [item setEnabled: YES];
        return YES;
    }
    if ([item action] == @selector(newDocument)) {
        return YES;
    }
    return YES;
}

@end
