//
//  TimerDocument.h
//  KitchenTimer
//
//  Created by Lewis Garrett on Fri May 30 2003.
//  Copyright (c) 2003 Iota. All rights reserved.
//


#import <Cocoa/Cocoa.h>

@class NamedTimerSettings;							//leg20110222 - 1.0.1

@interface TimerDocument : NSDocument
{
    long secondsTime;                                                           //leg20240403 - AS-1.3.0
    long minutesTime;                                                           //leg20240403 - AS-1.3.0
    long hoursTime;                                                             //leg20240403 - AS-1.3.0
    long lastCountDownTime;                                                     //leg20240403 - AS-1.3.0

    BOOL timerIsRunning;
    BOOL timerHasExpired;
    BOOL timerIsCountingUp;
    BOOL timerJustCountedDown;
    NSTimer *timer;
    NSSound *alarmSound;
    NSWindowController *myWindowController;
    NamedTimerSettings *namedTimerSettings;					//leg20110222 - 1.0.1
	
	NSMutableString *windowTitle;							//leg20110222 - 1.0.1
	NSMutableString *timerSound;							//leg20110222 - 1.0.1
	NSMutableString *timerName;								//leg20110222 - 1.0.1
	BOOL isNamedTimer;										//leg20110222 - 1.0.1
	
    // Timer initialization.                                                    //leg20240403 - AS-1.3.0
    NSDate  *timerStartDate;
    NSDateInterval *timerDateInterval;
    
    // Outlets
    IBOutlet NSTextField *timerDisplay;
    IBOutlet NSTextField *statusDisplay;    
    IBOutlet NSButton *hoursButton;
    IBOutlet NSButton *minutesButton;
    IBOutlet NSButton *secondsButton;
    IBOutlet NSButton *startStopButton;
    IBOutlet NSButton *resetButton;
}

// Action methods
-(IBAction)incrHours:(id)sender;
-(IBAction)incrMinutes:(id)sender;
-(IBAction)incrSeconds:(id)sender;
-(IBAction)startStopTimer:(id)sender;
-(IBAction)resetTimer:(id)sender;
-(IBAction)openWebSiteInDefaultBrowser:(id)sender;
-(IBAction)showNamedTimerSettingsWindow:(id)sender;		                        //leg20110222 - 1.0.1

// Private methods
- (void)updateTimerDisplay;
- (void)seeIfTimeIsUp:(NSTimer *)aTimer;

@end
